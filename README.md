# OpenWrt Wiki

A step-by-step guide to setup an OpenWrt based embedded distribution similar to the one used in SHAULA-720 or SHERATAN-990.

## The basics

OpenWrt is an Embedded Linux distribution based on [Buildroot](https://buildroot.org/). It is essentially a set of Makefiles, patches and tools which allows to generate a bootable Linux environment using cross compilation.
OpenWrt uses the Linux kernel, util-linux, musl (libc), and BusyBox (rootfs). All of these can be customized. 


## Getting started

OpenWrt only supports linux (IIRC) currently. So download and install a linux distribution. 

**Note:** 
* This guide assumes a working Ubuntu dev setup.
* Information contained in this section can be found in the [OpenWrt docs here](https://openwrt.org/docs/guide-developer/start).


### Install dependencies

```
sudo apt-get install subversion g++ zlib1g-dev build-essential git python rsync man-db
sudo apt-get install libncurses5-dev gawk gettext unzip file libssl-dev wget zip
```

### Get the sources

```
git clone https://git.openwrt.org/openwrt/openwrt.git <path-to-directory>
cd <path-to-directory>
```

### Install feeds

**Note:** Feeds installed here will show up in the `menuconfig` later. 

```
./scripts/feeds update <-a | pkg-name>
./scripts/feeds install <-a | pkg-name>
```


## Build minimal

The minimum steps required to get a working image are as below:

```
make menuconfig
# Set "Target System" option -> eg. (Allwinner A1x/A20/A3x) for SHAULA-720
# Set "Target Profile" option -> eg.(Olimex A20-Olinuxino Micro) for SHAULA-720
# Save & exit 
make # This will take a long time...
```

If all goes well you should have working images in the  `./bin/targets/<target-name>/generic/` directory.
The *\*-factory.bin* images are for the first installation.
The *\*-sysupgrade.bin* images are for the updating existing OpenWrt installations.


## Build custom

There are a lot of build options and this can be overwhelming at first. A good place to start would be to explore all the menu options in `make menuconfig` (for OpenWrt) and `make kernel_menuconfig` (for the Linux kernel).

Start by installing the feeds and run `make menuconfig`. Then set `Target System` and `Target Profile` as usual.

### Selecting packages

```
<> : Unselected package
<*>: Selected package
-*-: Selected due to dependency on another package
```